# Communication Subsystem

# 1. Introduction.

This chapter will discuss about the communication system on THAIIOT satellite. Which this subsystem serves to receive and transmit data between satellite and ground station. The designing and building to support VHF, UHF and S-Band frequency ranges. The ICD is describing the channel for communicate with communication module by I2C, UART and RS-485. In addition, the antennas are demonstrated by simulation with material and adjusted for the satisfactory results when compared with the measurement results. 

# 2. Abbreviated term.

        2.1 VU Transceiver
            2.1.1 VHF Receiver
            2.1.2 UHF transmitter
        2.2 S-Band Transmitter
            2.2.1 Master-Slave Communication Channel Description
            2.2.2 User Protocol
        2.3 Antenna Deployment
            2.3.1 Part of Deployment
            2.3.2 Part of RF Feeding

# 3. Subsystem Requirement.

Designing and building for THAIIOT communication systems, the data that will be sent to the ground station (TX Downlink) consists of 2 parts:

        •	Data status of satellite (Telemetry Data), which transmitted at Continuous Wave (CW) Beacon at UHF frequency band. The data is in
        form of Morse code and the modulation is BPSK-G3RUH (Binary phase Shift Keying with Scrambling) at UHF frequency band.  The data is in
        the format AX.25 Protocol and the bit rate (Bit Rate) of data transmission is 9,600 bps.
        •	Stored and Forward data will be sent by QPSK (Quadrature Phase Shift Keying) signal modulation at S-Band frequency. Data Format is
        in form of CCSDS Protocol and bit rate of data transmission is in the range of 100 kbps to 4 Mbps.

For command data to receive from ground station (Rx Uplink) will be sent in form of FSK-G3RUH modulation (Frequency Shift Keying with Scrambling) in VHF band. The data is in the format AX.25 Protocol and the bit rate (Bit Rate) of data transmission is 9,600 bps.
THAIIOT Communication board is a CubeSat transceiver module designed specifically for nanosatellite applications. The transmitter frequency is programmable at UHF frequencies from 430 MHz to 450 MHz and supports BPSK. The receiver frequency is programmable at VHF frequencies from 140 MHz to 150 MHz and supports AFSK. Frequencies outside the above-mentioned ranges are available upon request.
A typical application of the S-Band transmitter was shown on Figure 2. Two separate connectivity sides can be distinguished – HOST (OBC) side and Antenna side. Hence, two separate interfaces – HOST (OBC) interface and Downlink interface using 2250 MHz frequency with QPSK, 8-PSK or 16-APSK modulations.
However, antenna is an essential device to receive data into receiver module. Then, the transmitter sends the data via antenna to others receiver. The main functionality of the antenna system is to deploy the stowed antennas so that the antennas can be used for RF transmissions.

